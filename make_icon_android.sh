#!/bin/sh
#---------------------------------------------------------------
# Given an xxhdpi image or an App Icon (launcher), this script
# creates different dpis resources
#
# Place this script, as well as the source image, inside res
# folder and execute it passing the image filename as argument
#
# Example:
# ./drawables_dpis_creation.sh ic_launcher.png
# OR
# ./drawables_dpis_creation.sh my_cool_xxhdpi_image.png
#---------------------------------------------------------------

FILE=$(basename $1)

echo " Creating different dimensions (dips) of "$FILE" ..."

if [[ -a ./drawable-xxhdpi && -d ./drawable-xxhdpi ]]
then
    if [[ $(file $1 | grep PNG) ]]
    then
	echo "Making xxhdpi"
	convert $1 -resize 144x144 drawable-xxhdpi/$FILE
	echo "Making xhdpi"
	convert $1 -resize 96x96 drawable-xhdpi/$FILE
	echo "Making hdpi"
	convert $1 -resize 72x72 drawable-hdpi/$FILE
	echo "Making mdpi"
	convert $1 -resize 48x48 drawable-mdpi/$FILE
	echo " Done"
    else
	echo "$1 not a PNG file"
    fi
else
    echo "Not in correct directory, go to right directory (app/src/main/res)"
fi
