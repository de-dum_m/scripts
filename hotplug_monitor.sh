#!/bin/bash

export DISPLAY=:0
export XAUTHORITY=/home/de-dum_m/.Xauthority

function connect(){
xrandr --output VGA-0 --auto --left-of LVDS
}

function disconnect(){
xrandr --output VGA-0 --off
}

xrandr | grep "VGA-0 connected" &> /dev/null && connect || disconnect
